
define( [ "lodash" ], function( _ ) {
    "use strict";

    /**
     * TODO
     */

    var NAME = "moduleNameFetcher"
    ,type = function ( test ) {
        return ( {} ).toString.call( test ).match( /\s([a-zA-Z]+)/ )[ 1 ].toLowerCase();
    }
    ,getName = function( $getObj ) {

        var name;

        if( type( $getObj ) === "array" ) {
            var meth = _.where( $getObj, function( item ) { return (typeof item === "function"); })[0];

            // console.log( meth().name );

            name = meth().name;
        } else {
            name = $getObj().name;
        }

        return name;
    }

    return {

        

        /** 
         * Don't inject any deps unless you really need to, because gets called before app has been initialised,
         * which means they need to be read without Angular.
         */
        $get: function() {


            return {

                // Return name for the Services Module to retrieve
                name: NAME

                ,getModuleNames: function( args, isService ) { // "isService" gets used in unit tests

                    var rtnArr = [];
                    _.forEach( args, function( arg ) {

                        // if( !arg ) console.log("no arg");

                        // Uses 'factory' to be sure that we are using an Angular module. Could use any module property though.
                        var modulePass = (arg && type( arg ) === "object" && arg.factory && arg.name);

                        if( modulePass ) {
                            rtnArr.push( arg.name );
                        } else if( isService === true && arg && type( arg ) === "object" && arg.$get ) {

                            // Uses 'name', if it exists. This is not a standard Angular Service convention though.
                            rtnArr.push( getName(arg.$get) );
                        }
                    });

                    return rtnArr;
                }


                ,getServiceName: function(arg) {

                    if( type(arg) === "object" && arg.$get )
                        return getName(arg.$get);

                    if(console && console.warn)
                        console.warn( "Sorry, you must provide a valid '$get' based service for this utility. "+ NAME +" -> getServiceName." );
                }
                

                ,isService: function( val ) {

                    return (type(val) === "object" && !!(val.$get));
                }
            }
        }
    }
});