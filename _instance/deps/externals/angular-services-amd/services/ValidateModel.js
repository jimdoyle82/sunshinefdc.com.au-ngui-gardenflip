
define( [ "angular", "lodash"], function( angular, _ ) {
    "use strict";

    /**
     * Service to check validity of Models.
     * Only works in ES5 browsers, but then you're probably debugging in one, right?
     */

    return {

        /** 
         * Don't inject any deps unless you really need to, because Models should to be able to read this, 
         * which are not specifically Angular functions, meaning they'd then need to parse an array.
         */
        $get: function() {

            return {

                // Return name for the Services Module to retrieve
                name: "validateModel"

                /**
                 * Method to check that an 'inheritedModel', passed into a Directive's isolated scope, is a 
                 * perfect shallow match to the accompanying Model. This is to avoid dynamic Model properties 
                 * that the Directive isn't expecting. What you see in the Model is what you should expect. 
                 * Also prevents extensibility by throwing an error if you attempt to add a new property.
                 */
                ,directiveModel: function( Mdl, baseName, scope, oneWay, inhMdlName ) {

                    // Uses default 'inhMdlName' if custom one not specified
                    if( !inhMdlName ) inhMdlName = "inhMdl";

                    // Get the 'inheritedModel', using different syntax, based on a Directive's 'oneWay' isolated scope properties.
                    var inhMdl = (oneWay ? scope[ inhMdlName ]() : scope[ inhMdlName ]);

                    // If the Directive's scope has been passed a model, check that it's shallow Object keys are identical to the directive's model.
                    if( inhMdl && Object.keys ) {

                        var shallowComparePassed = _.every( Object.keys( Mdl ), function( key ) {
                            return key in inhMdl;
                        });

                        if( shallowComparePassed === false ) {
                            if( console && console.error ) {
                                console.error(  "Mdl comparison failed on Directive "+ baseName +"!" + 
                                                " 'inheritedModel' must have same keys as the 'Mdl'." );
                            }
                            return null;
                        }
                    }

                    var mdl = inhMdl || angular.copy( Mdl );

                    if(Object.preventExtensions)
                        Object.preventExtensions(mdl);
                    
                    scope.Mdl = mdl;
                    
                    return mdl;
                }


                /**
                 * Prevents extensibility by throwing an error if you attempt to add a new property.
                 */
                ,pageModel: function( Mdl, scope ) {

                    var mdl = angular.copy( Mdl );

                    if(Object.preventExtensions)
                        Object.preventExtensions(mdl);

                    scope.Mdl = mdl;

                    return mdl;
                }
            }
        }
    }
});