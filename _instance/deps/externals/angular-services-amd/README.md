# Angular Services (RequireJS/AMD)

## Description
A collection of non-project specific AngularJS Services intended for sharing across multiple projects. Uses "$get" syntax and RequireJS/AMD "define" modules. 

## Dependencies
On top of RequireJS, dependencies may exist, such as LoDash and AngularJS.