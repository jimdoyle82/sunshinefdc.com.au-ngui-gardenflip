module.exports = function (grunt) {

    /**
     * Makes everything relative to the "deps" directory, so node_modules can go in "deps" folder.
     */
    // grunt.file.setBase( process.cwd() + "/deps" );


    grunt.initConfig({

        pkg: grunt.file.readJSON("package.json")


        /**
         * Using grunt-hub to call other Gruntfiles so we can keep our builds organised and portable.
         */
        ,hub: {
            options: { concurrent: 3 }
            ,svg: {
                src: "grunt_tasks/Gruntfile_svg.js"
                ,tasks: ["default"]
            }
            ,css: {
                src: "grunt_tasks/Gruntfile_css.js"
                ,tasks: ["nobanner"]
            }
        }


        ,copy: {
            singles: {
                expand: true
                ,flatten: false
                ,cwd: "../_dev/img/singles/"
                ,src: [  "**/*" ]
                ,dest: "../_dist/img/"
            }
        }

        /**
         * 'keepalive' should be false if 'livereload' is active.
         */
        ,connect: {
            options: {
                hostname: 'localhost'
            },
            dev: {
                options: {
                    keepalive: false,
                    livereload: true,
                    port: 9001,
                    base: "../",
                    open: 'http://<%= connect.options.hostname %>:<%= connect.dev.options.port %>/_instance/_dist/'
                }
            }
        }

        ,watch: {
            css: {
                options: {
                    /**
                     * Specify port so doesn't conflict with other watches
                     * Must include the script tag on your html page.
                     * <script src="//localhost:1337/livereload.js"></script>
                     * 
                     * Gotchas: If you're running this at the same time as another watch
                     * and are using the default port 35729, you'll get a script error 
                     * "GET http://localhost:35729/livereload.js?snipver=1 net::ERR_CONNECTION_REFUSED".
                     * This doesn't seem to affect anything though, so you can safely ignore it.
                     */
                    livereload: 1337
                },
                files: [
                    'grunt_tasks/forcereload.txt' // change this whenever you want to force a reload
                ]
            }
        }

    });
    

    grunt.registerTask("local", ["connect:dev", "watch"]);
    grunt.registerTask("css", ["hub:css", "copy:singles"]);
    grunt.registerTask("svg", ["hub:svg"]);

    grunt.registerTask("default", function(){

        grunt.log.writeln(""); // line break

        grunt.log.writeln("Builds for example only!".yellow.bold); 

        grunt.log.writeln(""); // line break
        
        grunt.log.writeln("grunt css".yellow); // line break
        grunt.log.writeln("Will build the example css".grey); 

        grunt.log.writeln(""); // line break        
        
        grunt.log.writeln("grunt local".yellow); // line break
        grunt.log.writeln("Will run a local development server for this module.".grey);
    });


    grunt.loadNpmTasks('grunt-hub');
    grunt.loadNpmTasks('grunt-contrib-copy');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-contrib-connect');
};