
module.exports = function(grunt) {

    /**
     * Makes everything relative to the "root" directory
     */
    grunt.file.setBase( process.cwd() + "/.." );

    grunt.log.writeln( "CWD: ".yellow + process.cwd().green );
    
    
    grunt.initConfig({

        pkg: grunt.file.readJSON("package.json")

        ,vars: {
            banner_content: '<%= pkg.name %> - <%= pkg.version %> - ' + grunt.template.today("yyyy-mm-dd") + ' - <%= pkg.author.name %>'
        }


        ,clean: {
            options: { force:true }
            ,css: { src: "_dist/css/*" }
        }


        ,compass: {
            options: {
                config: "grunt_tasks/compassconfig.rb"
                ,outputStyle: "expanded" //"nested, expanded, compact, compressed"
                // ,relativeAssets: false
            }

            ,debug: {
                options: {
                    sassDir: "scss/"
                    ,cssDir: "_dist/css/"
                }
            }

            ,addBanner: {
                options: {
                     specify: "_dist/css/example.css"
                    ,banner: "/* <%= vars.banner_content %> */"
                }
            }
        }

        ,watch: {
            css: {
                files: [ "../_dev/**/*.scss" ]
                ,tasks: [ "compass:debug", "forcereload" ]
            }
        }

    });
        

    grunt.loadNpmTasks('grunt-contrib-compass');
    grunt.loadNpmTasks('grunt-contrib-clean');
    grunt.loadNpmTasks('grunt-contrib-watch');

    grunt.registerTask("default", [ "compass:debug", "compass-banner" ] );
    grunt.registerTask("nobanner", [ "compass:debug", "watch" ] );

    grunt.registerTask("compass-banner", "Forces compass task to add a banner despite warning", function() {
        grunt.option("force", true);
        grunt.task.run(["compass:addBanner"]);
        grunt.log.writeln("Don't worry about this warning: ".green + "'↑ Used'".green.bold + ". It's non-destructive. Forcing continue.".green );
    });


    grunt.registerTask("forcereload", "Change this file to force page refresh", function() {
        console.log( "Changing watched file: " + "grunt_tasks/forcereload.txt" );
        grunt.file.write( "grunt_tasks/forcereload.txt", Math.random() );
    });
};
